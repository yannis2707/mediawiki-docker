# Add this to the end of your LocalSettings.php file.

wfLoadSkin('MinervaNeue');

wfLoadExtension('VisualEditor');
$wgDefaultUserOptions['visualeditor-enable'] = 1;
$wgVirtualRestConfig['modules']['parsoid'] = array(
    'url' => 'http://localhost:8142',
    'domain' => 'localhost',
    'prefix' => ''
);
$wgSessionsInObjectCache = true;
$wgVirtualRestConfig['modules']['parsoid']['forwardCookies'] = true;

wfLoadExtension('UserMerge');
$wgGroupPermissions['bureaucrat']['usermerge'] = true;
$wgGroupPermissions['sysop']['usermerge'] = true;
$wgUserMergeProtectedGroups = array();

# MobileFrontend
wfLoadExtension('MobileFrontend');
$wgMFAutodetectMobileView = true;
$wgMFDefaultSkinClass = 'SkinMinerva';

# EmbedVideo
wfLoadExtension("EmbedVideo");
