#!/bin/bash

# Fix permissions of images folder
chown -R 999:999 /images /var/www/mediawiki/images

# Start supervisor
/usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord.conf