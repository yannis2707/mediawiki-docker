# MediaWiki-Docker

[![license MIT](https://img.shields.io/badge/license-MIT-green)](https://gitlab.com/yannis2707/mediawiki-docker/blob/master/LICENSE)
[![docker pulls](https://img.shields.io/docker/pulls/yannis2707/mediawiki-docker)](https://hub.docker.com/r/yannis2707/mediawiki-docker)
[![docker stars](https://img.shields.io/docker/stars/yannis2707/mediawiki-docker)](https://hub.docker.com/r/yannis2707/mediawiki-docker)

[![MediaWiki](https://gitlab.com/yannis2707/mediawiki-docker/raw/master/mediawiki.png)](https://www.mediawiki.org)

A simple [MediaWiki](https://www.mediawiki.org) container running under [Nginx](https://www.nginx.com) and [PHP-FPM](https://php-fpm.org/).

## Features

- [MediaWiki](https://www.mediawiki.org) 1.32.2
- [Nginx](https://www.nginx.com)
- [PHP-FPM](https://php-fpm.org/) with [PHP7](https://www.mediawiki.org/wiki/Compatibility/de#PHP) 7.3.x
- [VisualEditor](https://www.mediawiki.org/wiki/VisualEditor) extension
- [UserMerge](https://www.mediawiki.org/wiki/Extension:UserMerge) extension
- [MobileFrontend](https://www.mediawiki.org/wiki/Extension:MobileFrontend) extension
- [EmbedVideo](https://www.mediawiki.org/wiki/Extension:EmbedVideo) extension
- [Parsoid](https://www.mediawiki.org/wiki/Parsoid) running on NodeJS 10.16.0 LTS
- Imagick for thumbnail generation
- Intl for Unicode normalization
- APC as in memory PHP object cache

## Docker Tags

- `1.32` [(Dockerfile)](https://gitlab.com/yannis2707/mediawiki-docker/blob/1.32/Dockerfile)

## Quick Setup

### 1. Install [Docker](https://docs.docker.com/install/) and [Docker Compose](https://docs.docker.com/compose/install/) if you haven't already done it.

### 2. Create a docker-compose.yml file with the following contents.

[Docker Compose Example](https://gitlab.com/yannis2707/mediawiki-docker/blob/master/example/docker-compose.yml)

```
version: '3'
services:
  mediawiki:
    image: yannis2707/mediawiki-docker
    container_name: mediawiki
    ports:
    # host:container
    - 8080:8080
    volumes:
    - mediawiki_images:/images
    - mediawiki_data:/data
    # Uncomment the next line after you put LocalSettings.php in the same directory as this file.
    # - ./LocalSettings.php:/var/www/mediawiki/LocalSettings.php
volumes:
  mediawiki_images:
  mediawiki_data:
```

### 3. Create and start the Docker container with ```sudo docker-compose up```.

### 4. Access the MediaWiki installation page under http://localhost:8080 and follow the steps. (SqlLite directory should be ```/data```)

### 5. Stop the Docker container with Ctrl-C.

### 6. Download your LocalSettings.php file and add the contents of [AddThisToLocalSettings.php](https://gitlab.com/yannis2707/mediawiki-docker/blob/master/example/AddThisToLocalSettings.php).

### 7. Put your LocalSettings.php file in the same directory as the docker-compose.yml file and uncomment line 13.

### 8. Recreate the Docker Container with ```sudo docker-compose up --no-start``` and start it with ```sudo docker-compose start```.

## Security

* Nginx and PHP-FPM worker processes run under the `www-data` user with UID 999 and GID 999.
* Parsoid runs under the `parsoid` user.
* Parsoid runs only inside the container. There is no port exposed.
* The MediaWiki files are owned by `root`. Only the `images` folder is owned by `www-data`.
* The Parsoid files are all owned by `root`.

## Issues

Report issues [here](https://gitlab.com/yannis2707/mediawiki-docker/issues).

## License

This project is licensed under the MIT license.
See [LICENSE](https://gitlab.com/yannis2707/mediawiki-docker/blob/master/LICENSE) file.