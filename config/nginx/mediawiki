server {
    listen 8080;
    listen [::]:8080;

    root /var/www/mediawiki;
    index index.php index.html;
    server_tokens off;
    client_max_body_size 5m;
    client_body_timeout 60;

    location ~ \.htaccess {
        deny all;
    }

    location / {
	    try_files $uri $uri/ @rewrite;
    }

    location @rewrite {
        rewrite ^/(.*)$ /index.php;
    }

    location ^~ /maintenance/ {
        internal;
    }

    location ~* \.(js|css|png|jpg|jpeg|gif|ico)$ {
        try_files $uri /index.php;
        expires max;
        log_not_found off;
    }

    location = /_.gif {
        expires max;
        empty_gif;
    }

    location ^~ /cache/ {
        internal;
    }

    location ~ \.php$ {
        fastcgi_pass unix:/var/run/php7-fpm/mediawiki.socket;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include fastcgi_params;
        fastcgi_param PATH_TRANSLATED $document_root$fastcgi_script_name;
        fastcgi_param HTTPS off;
        fastcgi_index index.php;
    }
}